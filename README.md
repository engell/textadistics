
# Textadistics

Una sencilla herramienta escrita en Python que permite analizar textos y el proceso de escritura de los mismos en varias distribuciones de teclado como Qwerty o Dvorak.

## ToDo

 - [x] Leer archivo
 - [x] Analizar texto dentro del archivo
   - [x] Verificar caracteres y sus repeticiones
   - [x] Verificar palabras y sus repeticiones
   - [ ] Verificar párrafos
 - [x] Generar visualización gráfica de caracteres tipeados en distintas distribuciones de teclado
   - [x] Dvorak
   - [x] Qwerty
   - [ ] Azerty
 - [ ] Mostrar palabras más repetidas y palabras contiguas
 - [x] Generar interfaz de usuario

## Ejemplos

Visualizando los carácteres tipeados del texto de ejemplo con diferentes distribuciones de teclado:

![Dvorak layout](images/dvorak.png "Dvorak")

![Qwerty layout](images/qwerty.png "Qwerty")

## Instalación

Clonar el repositorio

```
git clone https://gitlab.com/engell/textadistics.git
```

Entrar en la carpeta del proyecto

```
cd textadistics
```

Instalar virtualenv

```
pip3 install virtualenv
```

Crear el entorno virtual

```
virtualenv env -p python3.9.2
```

Activar entorno virtual

```
source env/bin/activate
```

Instalar requisitos 

```
pip3 install -r requirements.txt
```

## Modo de uso

Asegurarse de estar en la misma carpeta que se encuentra el archivo `textadistics.py`.

```
cd textadistics
```

Para visualizar las teclas pulsadas al escribir el texto de ejemplo con teclado dvorak en español:

```
python3 textadistics.py -s dvorak_es test/lorem_ipsum.txt
```

Para ver el mismo texto si se escribiera con qwerty:

```
python3 textadistics.py -s qwerty_es test/lorem_ipsum.txt
```

Se puede guardar la imágen en un archivo con la opción -w como se ve a continuación:

```
python3 textadistics.py -s dvorak_es test/lorem_ipsum.txt -w test/dvorak_es.png
```

Mostrar una lista de los caracteres tipeados en el texto junto con el número de repeticiones:

```
python3 textadistics.py -r test/lorem_ipsum.txt
```

Listar las palabras y número de repeticiones:

```
python3 textadistics.py -rr test/lorem_ipsum.txt
```

## Licencia

Este proyecto se encuentra bajo licencia MIT.

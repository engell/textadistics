import cv2
import numpy
import random
from keyboard_layouts import *

key_zone = {}
keyboard_canvas = numpy.zeros((430, 1215, 3), numpy.uint8)


def rain(text):
    def random_point(coords):
        x = random.randint(coords[0], coords[1])
        y = random.randint(coords[2], coords[3])
        return (x - 6, y + 2)

    for i in text:
        # TODO: Agregar teclas especiales y mayúsculas. (enter, shift, acento, etc.)
        if key_zone.get(i[0]):
            zone = key_zone[i[0]]
            for n in range(i[1]):
                random_coords = random_point(zone)
                cv2.putText(keyboard_canvas, ".", (random_coords), 2, 1, (0,0,255), 1, cv2.LINE_AA)
            cv2.putText(keyboard_canvas, str(i[1]), (zone[0], zone[3]), 1, 1, (0,255,0), 1, cv2.LINE_AA)


def create_keyboard(layout, text_radiography):
    global key_zone
    cv2.rectangle(keyboard_canvas, (0, 0), (1214, 429), (100, 100, 100), -1)
    cv2.rectangle(keyboard_canvas, (0, 0), (1214, 429), (50, 50, 50), 10)

    for i in keyboard_blank:
        key_id = layout[i]
        pos = keyboard_blank[i]
        cv2.rectangle(keyboard_canvas, (pos[0], pos[2]), (pos[1], pos[3]), (50, 50 ,50), -1)
        pos_x = int((pos[0] + pos[1]) / 2)
        pos_y = int((pos[2] + pos[3]) / 2)

        key_zone.setdefault(key_id, pos)
        
        if len(key_id) != 1:
            cv2.putText(keyboard_canvas, str(key_id), (pos_x - 30, pos_y + 10), 1, 1, (100, 100, 100), 1, cv2.LINE_AA)
        else:
            cv2.putText(keyboard_canvas, str(key_id), (pos_x - 10, pos_y + 10), 2, 1, (100, 100, 100), 1, cv2.LINE_AA)

    if text_radiography:
        rain(text_radiography)


def show_keyboard():
    cv2.imshow("Imagen", keyboard_canvas)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    

def save_image(out_file):
    cv2.imwrite(out_file, keyboard_canvas)
    cv2.waitKey(0)

from sys import stderr
from pathlib import Path
import argparse

from word_processing import analyze
from draw_keyboard import *


class CpError(Exception):
    pass


def cli() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        prog = "textadistics",
        description="A simple text analyzer",
    )
    parser.add_argument(
        "file",
        type = Path,
        help = "File to analyze"
    )
    parser.add_argument(
        "-i", "--interactive",
        action = "store_true",
        help = "Execute interactive cli",
    )
    parser.add_argument(
        "-r", "--radiography",
        action = "count",
        default = 0,
        help = "Show a basic radiography of the text",
    )
    parser.add_argument(
        "-s", "--show",
        choices = ["dvorak_es", "qwerty_es"],
        default = 0,
        help = "Show a keyboard layout with the characters typed on te text",
    )
    parser.add_argument(
        "-w", "--write",
        #type = Path,
        default = 0,
        help = "Save an image from the keyboard view with -s option"
    )

    return parser.parse_args()


def interactive():
    # TODO Terminar interface interactiva.
    welcome="""
        Bienvenido a Textadistics, selecciona lo que deseas hacer.

        1.- Leer un archivo de texto para analizar.
        0.- Salir
    
    """
    
    selection = input(welcome)
    
    if int(selection) == 1:
        readfile("textadistics/test/neurogamer.md")
    elif int(selection) == 0:
        exit()
    else:
        print("Opción no válida")
    intro()


def readfile(text_file):
    text = open(text_file, "r")
    content = text.read()
    text.close()
    return content


def radiography(times, text):
    if times == 1:
        for i in text["characters"]:
            # TODO Agregar referencia a ENTER y SPACE_BAR
            print(i[0], i[1])
    elif times == 2:
        for i in text["words"]:
            print(i[0], i[1])
    elif times == 3:
        print(text["original"])
    else:
        print(text["original"], "\n", text["words"], "\n \n", text["characters"])


def show(layout, text):
    if layout == "qwerty_es":
        create_keyboard(qwerty_esp, text)
    elif layout == "dvorak_es":
        create_keyboard(dvorak_esp, text)
    else:
        print("I don't have this layout: ", layout)

    show_keyboard()


def main():
    args = cli()

    try:
        if not args.file.absolute().is_file():
            raise CpError("Destination is not a file.")
    except CpError as e:
        print(e, file=stderr)
        exit(1)

    text = readfile(args.file.absolute())
    text_info = analyze(text)

    if args.radiography:
        radiography(args.radiography, text_info)

    if args.show:
        show(args.show, text_info["characters"])

    if args.write:
        save_image(args.write)

if __name__ == '__main__':
    main()



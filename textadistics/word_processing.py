def analyze(text):
    chars = {}
    words = {}
    
    # Separe text in words and add to te word dictionary
    char_to_del = {ord(c): " " for c in (
        "?", "¿", "¡", "!", " ", ",", ".", ";", ":", "\n", "\"", "'", "“", 
        "”", "/", "-", "<", ">", "+", "=", "{", "}", "@", "[", "]", "’", 
        "‘", "~", "\\", "¨"
        )}
    only_words = text.translate(char_to_del)
    brute_words = only_words.lower().split(" ")
    for i in brute_words:
        word = i.replace(" ", "")
        if word == "":
            # Skip empty strings
            pass
        elif words.get(word):
            count = int(words.get(word)) + 1
            words.update({word: count})
        else:
            words.setdefault(word, 1)

    # Separe text in characrers and add to te chars dictionary
    a,b = "áéíóú", "aeiou"
    for i in text.lower():
        no_tilde = str.maketrans(a,b)
        c = i.translate(no_tilde)
        if chars.get(c):
            count = int(chars.get(c)) + 1
            chars.update({c: count})
        else:
            chars.setdefault(c, 1)

    # Sort both dictionaries by repeatition cases.
    sort_chars = sorted(chars.items(), key=lambda x: x[1], reverse=True)
    sort_words = sorted(words.items(), key=lambda x: x[1], reverse=True)

    # Prepare the return data.
    text_info = {}
    text_info["original"] = text
    text_info["words"] = sort_words
    text_info["characters"] = sort_chars

    return text_info


def debug():
    test_text = """Neurogamer
Anoche terminé de leer el libro que da título a este post. Se trata de un libro escrito por un neurocientífico que está muy interesado por la divulgación. En el cuál intenta traer varios puntos a la mesa, desde el clásico: ¿Los videojuegos nos hacen violentos? hasta unas más nuevas como: ¿Puede un videojuego ayudar a entrenar nuestro cerebro?. El autor va alternando entre datos científicos y de cultura gamer, lo que hace que la lectura sea bastante dinámica y sencilla."""
    text_radiography = analyze(test_text)
    print(text_radiography["characters"])


if __name__ == "__main__":
    debug()
